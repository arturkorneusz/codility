def solution(juice, capacity):
    slots = []
    tmp = 0
    count = 1
    new_max = 0
    
    for i in range(len(juice)):
        slots.append(capacity[i] - juice[i])
    
    if pattern_check(slots) != 3:
        for i in range(len(slots)):
            if slots[i] != max(slots) and slots[i] != 0:
                new_max = max(slots) - slots[i]
                slots[i] = 0
                tmp = 1
                count = count + 1
            elif slots[i] == max(slots) and slots[i] != 0:
                if tmp == 1:
                    slots[i] = new_max
                    tmp = 0
           
    if count == 1 and len(slots) == 4:
        return 2
    else:
        return count

def pattern_check(arr):
    zero_count = 0
    for i in range(len(arr)):
        if arr[i] == 0:
            zero_count += 1
    return zero_count

def test_solutions():
    assert solution([10, 2, 1, 1],[10, 3, 2, 2]) == 2
    assert solution([1, 2, 3, 4],[3, 6, 4, 4]) == 3
    assert solution([2, 3],[3, 4]) == 1
    assert solution([1, 1, 5],[6, 5, 8]) == 3
    
'''
print(solution([10, 2, 1, 1],[10, 3, 2, 2]))    #2
print(solution([1, 2, 3, 4],[3, 6, 4, 4]))      #3
print(solution([2, 3],[3, 4]))                  #1
print(solution([1, 1, 5],[6, 5, 8]))            #3
'''
